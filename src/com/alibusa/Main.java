package com.alibusa;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        int year;

        System.out.println("Please input year: ");
        year = appScanner.nextInt();

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    System.out.println(year + " is a leap year.");
                } else {
                    System.out.println(year + " is NOT a leap year.");
                }
            } else {
                System.out.println(year + " is a leap year.");
            }
        } else {
            System.out.println(year + " is NOT a leap year.");
        }

    }
}
